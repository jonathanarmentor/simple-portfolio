import React, { useState } from 'react'
import styled from 'styled-components'
import Wrapper from './Wrapper'


const Experience = () => {
  const [value, changeValue] = useState('')
  const [items, changeItems] = useState(defaultValues)
  const handleValueChange = e => {
    changeValue(e.target.value)
    changeItems(
      e.target.value === ''
        ? defaultValues
        : defaultValues.filter(
          item =>
            item.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
        )
    )
  }

  return (
    <Wrapper as="main">
      <h1>Experience</h1>
      <input
        value={value}
        onChange={handleValueChange}
        type="text"
        id="experienceSearch"
      />
      <label htmlFor="experienceSearch">filter experience</label>
      <ExperienceElements items={items} />
    </Wrapper>
  )
}

const ExperienceElements = ({ items, ...props }) => (
  <TagGroup {...props}>
    {items.map((item, key) => (
      <Tag key={key}>
        <a href={`#${item}${key}`}>{item}</a>
      </Tag>
    ))}
  </TagGroup>
)


const Tag = styled.li`
  display: inline;
  padding: 0.5em 1em;
  margin: 1em;
  border-radius: 0.25em;
  background-color: #ccc;
  transition: all 300ms ease;
  border: 2px solid transparent;
  user-select: none;
  will-change: border-color background-color;
  a {
    color: var(--baseWhite);
    transition: 300ms ease;
    text-decoration: none;
    will-change: color;
  }

  &:hover {
    border-color: var(--baseBlack);
    background-color: transparent;

    & > a {
      color: var(--baseBlack);
    }
  }
`
const TagGroup = styled.ul`
  max-width: 60vmax;
  flex-wrap: wrap;
  align-items: center;
  flex-direction: row;
  padding: 0;
  display: flex;
  justify-content: center;
`

const defaultValues = [
  'HTML',
  'CSS',
  'JavaScript',
  'ES6',
  'node',
  'bash',
  'PHP',
  'Docker',
  'Wordpress',
  'React',
  'Bootstrap',
  'Ant.design',
  'Materialize',
  'Webpack',
  'Apache',
  'Nginx',
  'GraphQL',
]
export default Experience
