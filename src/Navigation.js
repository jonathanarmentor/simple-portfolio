import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom';
import { ReactComponent as Bee } from './bee_bw.svg';

const NavItem = ({ to, children, ...props }) => (
  <Item {...props}>
    <Link to={to} >{children}</Link>
  </Item>
)

export default function Navigation({ items }) {
  return (
    <Nav>
      <ul>
        <NavItem to="/">

          <Bee height="2em" />
        </NavItem>
        <NavItem to="/story" >Story</NavItem>
        <NavItem to="/experience">Experience</NavItem>
        <NavItem to="/contact">Contact</NavItem>
      </ul>
    </Nav>
  )
}
const Nav = styled.nav`

position: sticky;
top: 0;
z-index: 2;

ul {
  height: 3rem;
  display: flex;
  justify-content: space-around;
  align-items: center;
  background-color: var(--baseWhite);
  padding-top: 1rem;
  padding-bottom: 1rem;
  list-style:none;
  margin: 0;


  
  @media(max-width: 768px) {
    flex-direction: column;
    border-bottom: 1px solid white;
    height: auto;

    margin:0;
    padding:0;
    
    
    li {
      margin: 2em;
    }

  }
}
`

const Item = styled.li`

svg {
  fill: var(--baseBlack);
  display: inline;

  :hover {
      fill: var(--accentYellow);
      }
  
}


svg:first-of-type {
    flex-basis: .6s;
    :after {
      display: none;
    }
  }
a {
  color: var(--baseBlack);
  text-decoration: none;
  text-transform: uppercase;
  letter-spacing: 0.125em;
}


.activePage {
      font-weight:600;
      
    }
::after {
  content: " ";
  font-weight: 600;
  position: relative;
  top: .125em;
  display: block;
  height: 2px;
  width: 100%;
  transition: transform 150ms ease;
  transform: scale(0);
  transform-origin: bottom center;
  background-color: var(--accentYellow);
}

&:hover::after {
  transform: scale(1);
}
`
