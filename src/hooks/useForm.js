import { useState } from 'react'

const useForm = (initialValues, callback) => {
    const [values, setValues] = useState(initialValues)

    const submit = event => {
        event && event.preventDefault()
        callback()
    }

    const reset = () => setValues(initialValues)

    const change = event => {
        event.persist()
        setValues(values => ({
            ...values,
            [event.target.name]: event.target.value,
        }))
    }

    return {
        handlers: { submit, reset, change },
        values,
    }
}

export default useForm