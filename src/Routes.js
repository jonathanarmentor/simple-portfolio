import React from 'react';
import { Route } from 'react-router-dom';
import Experience from './Experience';
import ContactForm from './containers/ContactForm'

const Home = (props) => (
    <div className="home"><h1>Home</h1>{props.children}</div>
)
const Story = (props) => (
    <div className="story"><h1>My Story</h1>{props.children}</div>
)
const Contact = (props) => (
    <div className="Contact"><h1>Contact</h1><ContactForm endpoint="https://api.jonathanarmentor.com/api"/>{props.children}</div>

)


const Routes = () => (
    <>
        <Route path="/" exact component={Home} />
        <Route path="/experience" component={Experience} />
        <Route path="/story" component={Story} />
        <Route path="/contact" component={Contact} />
    </>
)
export default Routes;