import React from 'react'
import useForm from '../hooks/useForm'
import FormField from '../components/FormField'

const ContactForm = ({ endpoint }) => {
    const initialValues = { name: '', email: '', message: '' }
    const { values, handlers } = useForm(initialValues, () => {
        submitForm(endpoint, values, handlers.reset)
    })

    return (
        <form onSubmit={handlers.submit}>
            <FormField
                type="text"
                name="name"
                id="name"
                onChange={handlers.change}
                value={values.name}
                required
                autoFocus
                label="Name"
            />
            <FormField
                type="text"
                name="email"
                id="email"
                onChange={handlers.change}
                value={values.email}
                required
                label="Email"
            />

            <FormField
                as="textarea"
                name="message"
                onChange={handlers.change}
                value={values.message}
                required
                rows="10"
                cols="40"
                label="Message"
            />
            <input type="submit" value="submit" />
        </form>
    )
}

const submitForm = (endpoint, values, reset) => {
    fetch(endpoint, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ fields: values }),
    }).then(res => {
        if (res.ok) {
            reset()
            return true
        } else {
            console.log(res.statusText)
            // let err = new Error(res.statusText)
            // err.response = res
            // throw err
        }
    })
}

export default ContactForm
