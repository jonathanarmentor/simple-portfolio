import React from 'react';
import { ReactComponent as Bee } from '../bee_bw.svg';
import styled from 'styled-components';

const Footer = ({ copyrightDate, props }) => <StyledFooter>
    <p>
        &copy; Copyright {copyrightDate} Jonathan Armentor
    </p>
    <Bee />
</StyledFooter>

Footer.defaultProps = {
    copyrightDate: new Date().getFullYear()
}


const StyledFooter = styled.footer`
    display:flex;
    flex-flow: row-reverse;
    justify-content: space-around;
    min-height:30vh;
    background-color: var(--baseBlack);
    color: var(--baseWhite);
    align-items: center

    svg {
        height: 3em;
        fill: var(--accentYellow);
    }

    @media (max-width: 768px) {
        flex-flow: column;
    }
`

export default Footer;