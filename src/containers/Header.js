import React from 'react'
import styled from 'styled-components';
import headshot from '../headshot_masked.png';

const Header = ({ children }) => (
    <StyledHeader as="header">
        <img src={headshot} className="headshot" alt="Headshot" />

        <div>
            <h1>Hello!<br />I'm Jonathan</h1>

            <p>passionate designer and front-end developer.</p>
            <p>maker of hand-crafted digital experiences.</p>

        </div>
        {children}
    </StyledHeader>
)




const StyledHeader = styled.header`
    min-height: 40vh;
    overflow: hidden;
    max-width:100vw;
    background: var(--accentYellow);
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content:center;


    p {
        align-self:flex-end;
            font-size:3vmin;
        }


    @media (max-width: 768px) {
        flex-direction: column-reverse;
        justify-content: space-around;

        
        img.headshot {
            flex-shrink: .4;
            min-width:100%;
        }
    }

    >div{
flex-grow:1;
align-self:flex-end;
padding: 3em;
flex-flow: column;
position:sticky;
top:0;
align-items:center;
        background: white;

     
    }


    h1 {
        font-size: 10vmin;
    }
    
    img.headshot {
        filter: brightness(1.5) grayscale(1);
       flex-shrink: .3;
        mix-blend-mode: multiply;
        max-width: 45%;
        align-self:stretch;
        object-fit: cover;
        align-self:flex-start;
    }
    p {
        width: 36ch;
    }
    `

export default Header;