import React from 'react'
import styled from 'styled-components'

const FormField = ({ name, label, ...props }) => (
    <div>
        <StyledLabel htmlFor={props.id || name}>
            {label || props.children}
        </StyledLabel>
        <StyledInput name={name} id={props.id || name} {...props} />
    </div>
)


const StyledInput = styled.input`
    border-radius: 4px;
    display: block;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: box-shadow 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    will-change: box-shadow;
    border: 0;

    &:focus-within {
        outline: 0;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19),
            0 6px 6px rgba(0, 0, 0, 0.23);
    }
`

const StyledLabel = styled.label`
    font-weight: bold;
    margin: 0 1em;
    display: inline;
    &,
    ${StyledInput} {
        padding: 0.5em;
        width: 100%;
        margin: 1em 0;
    }

    & + input {
        margin-top: 2em;
    }
`

export default FormField
