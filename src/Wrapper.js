import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  min-height: 60vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  font-size: calc(10px + 2vmin);
`

const Wrapper = (props) => <Wrap {...props}/>


export default Wrapper;