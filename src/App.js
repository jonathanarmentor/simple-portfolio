import React from 'react'
import Navigation from './Navigation'
import Header from './containers/Header';
import Footer from './containers/Footer';
import Routes from './Routes';


function App() {
  return (
    <>
      <Header />
      <Navigation />
      <main>
        <Routes />
      </main>
      <Footer />
    </>
  )
}

export default App
