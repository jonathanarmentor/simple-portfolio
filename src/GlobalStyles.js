import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
:root {
  --baseWhite: #f9f9f9;
  --accentYellow: #ffcd67;
  --baseBlack: #231F20;
}

html {
  margin:0;
  padding:0;
  font-size: 14px;
  color: var(--baseBlack);
  background-image: linear-gradient(
    30deg,
    to top,
    #f9f9f9 0%,
    #f9f9f9 1%,
    whitesmoke 100%
  );
}

main {
  min-height: 100vh;
  background: var(--baseWhite);

}

body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  box-sizing: border-box;
  color: var(--baseBlack);
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}

`
export default GlobalStyles;
